declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module "vue-tel-input-vuetify/lib"

declare module "survey-knockout"

declare module "vue-nprogress"

declare module "vuejs-countdown-timer"

declare module "vuetify-google-autocomplete"

declare module "vuetify-image-input/a-la-carte"
