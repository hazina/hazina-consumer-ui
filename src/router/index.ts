import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Home from '../views/Home/Home.vue'
import App from '../views/Layouts/App/App.vue'
import Offer from '../views/Offer/Offer.vue'
let  Login =  import('../views/Login/Login.vue')
let  NewOffer =  import('../views/NewOffer/NewOffer.vue')
let  MyOffer =  import('../views/MyOffer/MyOffer.vue')
let  EarnPoints =  import('../views/EarnPoints/EarnPoints.vue')
let  Products =  import('../views/Products/Products.vue')
let  RedeemPoints =  import('../views/RedeemPoints/RedeemPoints.vue')
let  Shipping =  import('../views/Shipping/Shipping.vue')
let  Profile =  import('../views/Profile/Profile.vue')
let  OnBoardingSurvey =  import('../views/OnboardSurvey/OnboardSurvey.vue')
let  OnboardAccountUpdate =  import('../views/OnboardAccountUpdate/OnboardAccountUpdate.vue')
let  Logout =  import('../views/Logout/Logout.vue')
let  FreebieSurvey =  import('../views/FreebieSurvey/FreebieSurvey.vue')
let  FeedbackSurvey =  import('../views/FeedbackSurvey/FeedbackSurvey.vue')
let  SurveyNotify =  import('../views/SurveyNotify/SurveyNotify.vue')
let  Auth =  import('../views/Auth/Auth.vue')
let OnboardWelcome = import('../views/OnboardWelcome/OnboardWelcome.vue')
let Refer = import('../views/Refer/Refer.vue');
let FeedbackSurveys = import('../views/FeedbackSurveys/FeedbackSurveys.vue');


Vue.use(VueRouter)

const routes = [
    {
        path: '', 
        component:App,
        children: [
            {
                path: '/',
                name: 'Home',
                component:Home
            },
            {
                path: '/login',
                name: 'Login',
                component:()=> Login
            },
            {
                path: '/de-auth',
                name: 'Logout',
                component:()=> Logout
            },
            {
                path: '/my-offers',
                name: 'MyOffer',
                component:()=> MyOffer,
                meta: {
                    requiresAuth: true
                }
            }, 
            {
                path: '/new-offer',
                name: 'NewOffer',
                component:()=> NewOffer,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/earn-points',
                name: 'EarnPoints',
                component:()=> EarnPoints,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/products',
                name: 'Products',
                component:()=> Products,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/redeem-points',
                name: 'RedeemPoints',
                component:()=> RedeemPoints,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/shipping',
                name: 'Shipping',
                component:()=> Shipping,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/profile',
                name: 'Profile',
                component:()=> Profile,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/onboard/survey',
                name: 'OnBoardingSurvey',
                component:()=> OnBoardingSurvey,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/onboard/welcome',
                name: 'OnBoardingWelcome',
                component:()=> OnboardWelcome,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/onboard/settings',
                name: 'OnboardAccountUpdate',
                component:()=> OnboardAccountUpdate,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/survey/freebie/:id',
                name: 'FreebieSurvey',
                component:()=> FreebieSurvey,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/auth/:id',
                name: 'Auth',
                component:()=> Auth,
                meta: {
                    requiresAuth: false
                }
            },
            {
                path: '/feedback-surveys',
                name: 'FeedbackSurveys',
                component:()=> FeedbackSurveys,
                meta: {
                    requiresAuth: false
                }
            },
            {
                path: '/survey/feedback/:id',
                name: 'FeedbackSurvey',
                component:()=> FeedbackSurvey,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/survey/notify/:id/:type', 
                name: 'SurveyNotify',
                component:()=> SurveyNotify,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/campaign/:id/overview',
                name: 'OfferPage',
                component:()=>import("../views/Offer/Offer.vue"),
                meta: {
                    requiresAuth: true
                }
            }, 
            {
                path: '/refer-and-earn',
                name: 'Refer',
                component:()=>Refer,
                meta: {
                    requiresAuth: true
                }
            }, 
        ]
    },
    // {
    //     path: '/campaign/:id/overview',
    //     component: Offer,
    //     children: [
    //         {
    //             path: '',
    //             name: 'OfferPage',
    //             component:()=>import("../views/Offer/Offer.vue") 
    //         },
    //     ]
    // },
];

const router = new VueRouter({
    mode: 'history',
    // base: '/portals/consumer/',
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next()
            return
        }
        //next('/login')
        window.location.href = "https://gethazina.com/login?au=nx";
    } else {
        next()
    }
});

export default router
