import { Component, Prop, Vue } from 'vue-property-decorator';
var {VueperSlides, VueperSlide} = require("vueperslides");
import "vueperslides/dist/vueperslides.css";
import Axios from 'axios';
import Spinner from "@/components/Spinner/Spinner.vue";
import Empty from "@/components/Empty/Empty.vue";

@Component({
    components:{
        VueperSlides,
        VueperSlide,
        Spinner,
        Empty
    }
})
export default class MyOffer extends Vue {
 // @Prop() private msg!: string;
 my_offers : any = [];
 isLoading: boolean = false;
 statusColor: any  = {
     "PENDING": {color: "yellow", text_color: "black"},
     "ENDED":  {color: "red", text_color: "white"},
     "OPEN":  {color: "green", text_color: "white"},
 }

 mounted(){
    this.init();
}

 init(){
     this.isLoading = true;
    Axios.get("/consumer/load-my-offers")
    .then((resp: any)=>{
        this.isLoading = false;
       this.my_offers = resp.data.data;
       console.log(resp);
    })
    .catch((err)=>{
        this.isLoading = false;
        console.log(err);
    });
 }


}
