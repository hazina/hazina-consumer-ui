import { Component, Prop, Vue, Watch } from "vue-property-decorator";
import axios from "axios";
import * as _ from "lodash";
import Empty from "@/components/Empty/Empty.vue";
import { required, minLength } from "vuelidate/lib/validators";
import store from "@/store"

@Component({
    components: {
        Empty
    },
    validations: {
        shippingForm: {
            address: { required },
        }
    }
})
export default class Shipping extends Vue {
    hasError: boolean = false;
    showAlert: boolean = false;
    errorMessages: string = "";
    addressErrorMessage: Array<string> = [];
    dialog: boolean = false;
    isNotReady: boolean = true;
    isDetailsUpdating: boolean = false;

    shipping_detail: any = {};
    isShippingDetailLoading: boolean = false;
    shippingForm: any = {
        address: ""
    };

    isAwaitingLoading: boolean = false;
    awaiting_shipping: any = [];

    isDeliveredLoading: boolean = false;
    delivered_shipping: any = [];
    isStateSet: boolean = false;

    Lga: any = "";
    Area: any = "";
    addressData: any = "";

    state_name: string = "";

    states: any = [];
    lgas : any = [];
    areas : any = [];
    state: string = "";
   lga: string = "";
   area: string = "";
    street_number: string = "";
    street_name: string = "";
    landmark: string = "";
    is_home_address: boolean = true;

    get_address(){
        this.isShippingDetailLoading = true;
        axios.get("/consumer/load-shipping/profile")
        .then((resp)=>{
            this.states = resp.data.data.locations;
             this.isShippingDetailLoading = false;
             let details = resp.data.data.shipping_details;
             //this.state = _.find(this.states, {id: details.state_id});
             this.state = details.state_id;
             this.lgas =  _.filter(this.states, {parent_id: details.state_id});
             let getLgaChildren = _.filter(this.states, {parent_id: details.lga});
             this.areas =  getLgaChildren;
            // this.lga = _.find(this.states, {id: details.lga}); 
             this.lga = details.lga; 
             //this.area = _.find(this.states, {id: details.area}); 
             this.area = details.area; 
             this.street_number = details.street_number; 
             this.street_name = details.street_name; 
            this.shipping_detail = details;
            this.state_name = details.state_name;
            this.landmark = details.landmark;
            this.is_home_address = details.is_home_address;
            this.isStateSet = true;
        })  
        .catch((err)=>{
            this.isShippingDetailLoading = false;
            this.$toasted.error("Unable to loading shipping details").goAway(500)
        })
    }

    populateLga($evt: any){
        let id = $evt;
       this.lgas =  _.filter(this.states, {parent_id: id});
       this.areas = [];
       if(this.lgas.length > 0){
           this.isStateSet = true;
       }else{
        this.isStateSet = false;
       }
   
    }

    populateArea($evt: any){
        let id = $evt;
       this.areas =  _.filter(this.states, {parent_id: id});
    }
 
    getAddressData(evt: any, place: any){
        console.log(place);
        let address = place['address_components'];
        let getNebourhood = _.find(address, {types: ["neighborhood", "political"]});
        let getLga = _.find(address, {types: ["administrative_area_level_2", "political"]});
        
        if(getNebourhood === undefined){
            this.addressErrorMessage.push("Your address is not properly formed, it should be in this format: 35 Ayodele Odubiyi St, Eti-Osa, Lagos, Nigeria");
            this.isNotReady = true;
        }else{
            this.isNotReady = false;
            this.addressErrorMessage = [];
            this.Lga = getLga['long_name'];
            this.Area = getNebourhood['long_name'];
            this.addressData = JSON.stringify(place);
        }
    }

    updateShipping() {
        this.$v.shippingForm.$touch();
        this.isDetailsUpdating = true;
        //if (!this.$v.shippingForm.$invalid) {
            //var data = this.$v.shippingForm.$model;
            //console.log(this.$v.shippingForm.$model);
            var data = {
                state: this.state,
                lga: this.lga,
                area: this.area,
                street_number: this.street_number,
                landmark: this.landmark,
                street_name: this.street_name,
                is_home_address: this.is_home_address
            };
            axios
                .post("/consumer/update-shipping", data)
                .then(resp => {
                    this.isDetailsUpdating = false;
                    this.shipping_detail = resp.data.shipping_detail;
                    this.dialog = false;
                    this.$toasted.success("Shipping details updated successfully").goAway(1000)
                })
                .catch(err => {
                    this.isDetailsUpdating = false;
                    this.hasError = true;
                    this.showAlert = true;
                    if (
                        _.has(err.response.data, "errors") &&
                        typeof err.response.data.errors != "string"
                    ) {
                        var field = _.keys(err.response.data.errors);
                        this.errorMessages =
                            err.response.data.errors[field[0]][0];
                    } else {
                        this.errorMessages = err.response.data.errors;
                    }
                });
        // } else {
        //     this.isDetailsUpdating = false; 
        // }
    }

    get_awaiting(){
        this.isAwaitingLoading = true;
        axios.get("/consumer/load-shipping/awaiting")
        .then((resp)=>{
            this.isAwaitingLoading = false;
            this.awaiting_shipping = resp.data.data;
        })  
        .catch((err)=>{
            this.isAwaitingLoading = false;
            this.$toasted.error("Unable to loading awaiting items").goAway(500)
        })
    }

    get_delivered(){
        this.isDeliveredLoading = true;
        axios.get("/consumer/load-shipping/delivered")
        .then((resp)=>{
            this.isDeliveredLoading = false;
            this.delivered_shipping = resp.data.data;
        })  
        .catch((err)=>{
            this.isDeliveredLoading = false;
            this.$toasted.error("Unable to loading your delivered items").goAway(500)
        })
    }


    mounted(){
        this.get_address();
        this.get_awaiting();
        this.get_delivered();
    }

    get user() {
        return store.getters.user;
    }

    get addressErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.shippingForm.address.$dirty) return errors;
        !val.shippingForm.address.required &&
            errors.push("address is required.");
        return errors;
    }


}
