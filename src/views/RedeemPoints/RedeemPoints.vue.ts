import { Component, Prop, Vue } from 'vue-property-decorator';
import axios from "axios";
import * as _ from "lodash";
import Spinner from "@/components/Spinner/Spinner.vue";
import Empty from "@/components/Empty/Empty.vue";
import store from '@/store'
import { required, minLength, sameAs, email } from "vuelidate/lib/validators";
import { mdiDiamondOutline } from '@mdi/js';

@Component({
    validations: {
        rechargeForm:{
            network: { required },
            phone: { required },
        }
    },
    components:{
        Spinner,
        Empty
    }
})
export default class RedeemPoints extends Vue {
    freebies: any = [];
    //isLoading: boolean = false;

    totalData: number = 0;
    data: Array<object> = [];
    currentPage: number = 1;
    itemsPerPage: number = 0;
    loading: boolean = false;
    isSubmitting: boolean = false;
    totalPages: number = 0;

    currentFreebie: any;

    popUpTitle = "";
    popUpMessage = "";
    popUpUrl : any = "";
    popUpAction : any = false;
    dialog : any = false;
    isRedeeming: boolean = false;

    diamond: any = mdiDiamondOutline
    images: any = {
        cash: require("@/assets/images/cash.png"),
        airtime: require("@/assets/images/airtime.png"),
        coupons: require("@/assets/images/coupon.png"),
    }

    
    getData() {
        this.getDataFromApi()
            .then((resp: any) => {
                this.data = resp.data.data;
                // console.log(this.data);
            })
            .catch((error: any) => {
                this.$toasted.error("unable to fetch data");
            });
    }

    Paginate(page: any) {
        this.currentPage = page;
        this.getData();
    }

    updateBySearch() {
        this.getDataFromApi()
            .then((resp: any) => {
                this.data = resp.data.data;
                // console.log(this.data);
            })
            .catch((error: any) => {
                this.$toasted.error("unable to fetch data");
            });
    }

    mounted() {
        this.getData();
    }

    updateTable(options: any) {
        const page = options.page;
        if (page > this.currentPage || page < this.currentPage) {
            this.currentPage = page;
            this.getDataFromApi()
                .then((resp: any) => {
                    this.data = resp.data.data;
                    // console.log(this.data);
                })
                .catch((error: any) => {
                    this.$toasted.error("unable to fetch data").goAway(5000);
                });
        }
    }

    getDataFromApi() {
        this.loading = true;
        return new Promise((resolve, reject) => {
            // const { sortBy, sortDesc, page, itemsPerPage } = this.options
            axios
                .get("/consumer/load-redeem-points", {
                    params: { page: this.currentPage }
                })
                .then((resp: any) => {
                    let data = resp.data.data;
                    this.freebies = data.data;
                    // this.freebies = [];
                   // console.log(resp);
                    let items = data;
                    const total = data.total;
                    this.totalData = data.total;
                    this.currentPage = data.current_page;
                    this.itemsPerPage = data.per_page;
                    this.totalPages = data.last_page;
                    //console.log(data);
                    resolve({ data: items, total_data: data.total });
                    this.loading = false;
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    get user(){
        return store.getters.user;
    }

   RedeemPoints(freebie: any){
      if(this.user.points < freebie.points){
        this.$toasted.info("You do not have sufficient points to claim this Freebie. Please choose another freebie").goAway(6000);
      }else{
          this.dialog = true;
        
            if(freebie.type === 'cash' && this.user.account_number == null){
                this.popUpMessage = "You have not updated your bank account details! Please click the following button to update your account details";
                this.popUpTitle = 'One More Step!';
            //this.popUpUrl = this.$router.push({name: 'Profile'});
            this.popUpAction = false;
            this.currentFreebie = freebie;
            }else if((freebie.type === 'coupons' || freebie.type === 'airtime') && this.user.email_verified == 0){
                this.popUpMessage = "You have not verified your email address! Please check your inbox for the email verification link we sent you to verify your email address.";
                this.popUpTitle = 'One More Step!';
            //this.popUpUrl = this.$router.push({name: 'Profile'});
            this.popUpAction = false;
            this.currentFreebie = freebie;
            }else{
                this.popUpAction = true;
            this.popUpMessage = "Are you sure you want to redeem your point with "+ freebie.type+ " worth "+ freebie.amount + " Naira";
            this.popUpTitle = "Confirm!"
            this.currentFreebie = freebie;
            }
        
      }
   }

   triggerPopUpAction(){
        this.isRedeeming = true;
        let data = {
            id: this.currentFreebie.id
        }
        axios.post("/consumer/redeem-points", data)
        .then((resp)=>{
            this.currentFreebie = null;
            this.popUpAction = false;
            this.popUpMessage = "";
            this.popUpTitle = "";
            this.isRedeeming = false;
            this.$toasted.success("Your Request has been saved! We shall get back to you in no time.").goAway(5000);
            store.commit("set_user", resp.data.user);
            this.isRedeeming = false;
            this.dialog = false;
            this.getData();
        })
        .catch((err)=>{
            this.isRedeeming = false;
            this.$toasted.error("There was an error saving your request").goAway(5000);
        })
   }
}
