import { Component, Prop, Vue } from 'vue-property-decorator';
import Axios from 'axios';
import * as _ from 'lodash'
import Spinner from "@/components/Spinner/Spinner.vue";
import OnboardScreen from "@/components/OnboardScreen/OnboardScreen.vue";
import CryptoJsAesJson from "@/CryptoJsAesJson";
var {VueperSlides, VueperSlide} = require("vueperslides");
import store from "@/store";

@Component({
    components:{
        Spinner,
        OnboardScreen,
        VueperSlides,
        VueperSlide,
    }
})
export default class Home extends Vue {
    isLoading = false;
    data: any = [];
    user_acl: any = [];
    offersDialogLoading:boolean = false;
    Offersdialog:boolean = false;
    loadedOffers: any = [];
    selectedCategory: any = {};

   // user_acl_screen: number = 0;

    init(){
        this.isLoading = true;
        Axios.get("/consumer/load-home")
        .then((resp: any)=>{
            this.isLoading = false;
            this.data = resp.data.data;
            this.user_acl = JSON.parse(resp.data.data.user_acl);
            //this.user_acl_screen = parseInt(resp.data.data.user_acl) - 1;
        })
        .catch((err: any)=>{
            this.isLoading = false;
            //console.log(err);
        });
    }

    checkAcl(acl: Array<string>, isAll: boolean = false){
        //let supported = ['BASIC', 'EMAIL', 'PHONE', 'SURVEY'];
        let base = _.intersection(this.user_acl, acl);
        // console.log(base.length);
        if(base.length == acl.length){
           return true;
        }else{
            if(isAll && base.length == 1){
                return true
            }else if(isAll && base.length == 0){
                return false;
            }else{
                return false
            }
        }
    }

    showOffers(ID: any){
       this.offersDialogLoading = true;
       this.Offersdialog = true;
       //loadedOffers
       Axios.get("/consumer/load-home", {
           params:{
               action: 'offer',
               catId: ID
           }
       })
        .then((resp: any)=>{
            let dd = resp.data.data;
            this.loadedOffers = dd.campaigns;
            this.selectedCategory = dd.category;
        })
        .catch((err: any)=>{
           this.$toasted.error("Error showing offers");
        })
        .finally(()=>{
            this.offersDialogLoading = false;
        })
    }

    mounted(){
        this.init();
    }

    encrypt(str: any) {
        return CryptoJsAesJson.prototype.Encrypt(str);
      }

      public copyMessage(){
        let copyText = <any>document.getElementById("MessageInput");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand('copy');
        this.$toasted.success("Message has been copied to your clipboard").goAway(1500);
      }

      get user() {
        return store.getters.user;
      }
    

}
