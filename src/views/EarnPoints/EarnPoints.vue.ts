import { Component, Prop, Vue } from "vue-property-decorator";
import axios from "axios";
import * as _ from "lodash";
import Spinner from "@/components/Spinner/Spinner.vue";
import Empty from "@/components/Empty/Empty.vue";
import CryptoJsAesJson from "@/CryptoJsAesJson";

@Component({
  components: {
    Spinner,
    Empty,
  },
}) 
export default class EarnPoints extends Vue { 
  dailyLoading: boolean = false;
  freebies: any = [];
  can_play: boolean = false;
  daily_expiry: any = null;
  daily_start: any;

  headers: any = [
    { text: "Title", value: "survey.title", sortable: true },
    { text: "Questions", value: "survey.number_of_questions" },
    { text: "Points", value: "points", sortable: true, filterable: true },
    { text: "Actions", value: "id", sortable: false, filterable: false },
  ];


  getDailySurvey(){
    this.dailyLoading = true;
    axios
      .get("/consumer/load-earn-points/daily")
      .then((resp: any) => {
        this.dailyLoading = false;
        this.freebies = resp.data.data.daily_surveys;
        this.can_play = resp.data.data.can_play;
        this.daily_start = resp.data.data.start;
        this.daily_expiry = resp.data.data.expiry;
      })
      .catch((err: any) => {
        this.dailyLoading = false;
        this.$toasted.error("unable to fetching daily survey").goAway(5000);
      });
  }

  encrypt(str: any) {
    return CryptoJsAesJson.prototype.Encrypt(str);
  }

  mounted(){
    this.getDailySurvey();
  }


}
