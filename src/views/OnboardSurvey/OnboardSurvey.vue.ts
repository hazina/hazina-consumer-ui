import { Component, Prop, Vue } from "vue-property-decorator";
import Axios from "axios";
import Spinner from "@/components/Spinner/Spinner.vue";
import * as Survey from "survey-knockout";
require("survey-knockout/modern.css");
require("survey-knockout/survey.min.css");

@Component({
    components: {
        Spinner
    }
})
export default class OnboardSurvey extends Vue {
    survey: any = {};
    isLoading: boolean = false;
    isSaving: boolean = false;
    set_survey: any;
    survey_id: any = "";

    fetchSurveys() {
        this.isLoading = true;
        Axios.get("/consumer/load-onboard-survey")
            .then((resp: any) => {
                this.survey = resp.data.data.survey.survey;
                this.survey_id = resp.data.data.id;
                this.isLoading = false;
                Survey.StylesManager.applyTheme("orange");
                this.set_survey = new Survey.Survey(this.survey, "SurveyBox");
                this.set_survey.onComplete.add(this.sendDataToServer);
                this.set_survey.showCompletedPage = false;
            })
            .catch(err => {
                console.log(err);
                this.$toasted.error(
                    "Error fetching survey, please try reloading the page"
                );
                this.isLoading = false;
            });
    }

    mounted() {
        this.fetchSurveys();
    }

    sendDataToServer() {
        this.isSaving = true;
        let resultAsString = JSON.stringify(this.set_survey.data);
        Axios.post("/consumer/save-onboarding-survey", {
            survey: resultAsString,
            id: this.survey_id
        })
            .then((resp: any) => {
                this.isSaving = false;
                this.$router.push({ name: "OnBoardingWelcome" });
            })
            .catch(err => {
                this.isSaving = false;
            });
    }
}
