import { Component, Prop, Vue } from "vue-property-decorator";
import axios from "axios";
import * as _ from "lodash";
import Spinner from "@/components/Spinner/Spinner.vue";
import Empty from "@/components/Empty/Empty.vue";
import CryptoJsAesJson from "@/CryptoJsAesJson";
import store from "@/store";

@Component({
  components: {
    Spinner,
    Empty,
  },
})
export default class Refer extends Vue { 
  refMessage: string = "";

  totalData: number = 0;
  data: Array<object> = [];
  currentPage: number = 1; 
  itemsPerPage: number = 0;
  feedbackLoading: boolean = false;
  isPaginating: boolean = false;
  totalPages: number = 0;

  total_ref: any = 0;
  pending_points: any = 0;
  number_ref_points: any = 0;
            
  headers: any = [
    { text: "", value: "image", sortable: true },
    { text: "Name", value: "username", sortable: true },
    { text: "Date", value: "registered_on" },
    { text: "Points", value: "points", sortable: true, filterable: true },
    { text: "Registration Status", value: "reg_status", sortable: false, filterable: false },
  ];

  public copyMessage(){
    let copyText = <any>document.getElementById("MessageInput");
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand('copy');
    this.$toasted.success("Message has been copied to your clipboard").goAway(1500);
  }

  mounted(){
    // let Link = "https://gethazina.com/register?ref=";
    // this.refMessage = 'I just claimed a free product sample on Hazina. Sign up with my link to get 20points to start claiming yours.'+Link;
    this.initTable();
    this.getAnalytics();

  }

  get user() {
    return store.getters.user;
  }

  getDataFromApi() {
    this.feedbackLoading = true;
    return new Promise((resolve, reject) => {
      // const { sortBy, sortDesc, page, itemsPerPage } = this.options
      axios
        .get("/consumer/load-referrals/page", {
          params: { page: this.currentPage },
        })
        .then((resp: any) => {
          let data = resp.data.data;
          // console.log(resp);
          let items = data.data;
          const total = data.total;
         // this.data = items;
          this.totalData = data.total;
          this.currentPage = data.current_page;
          this.itemsPerPage = data.per_page;
          this.totalPages = data.last_page;
          //console.log(data);
          resolve({ data: items, total_data: data.total });
          this.feedbackLoading = false;
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  Paginate(page: any) {
    this.currentPage = page;
    //this.getData();
    this.isPaginating = true;
    axios
      .get("/consumer/load-referrals/page", {
        params: { page: this.currentPage },
      })
      .then((resp: any) => {
        let data = resp.data;
        const total = data.total;
        this.data = data.data;
        this.totalData = data.total;
        this.currentPage = data.current_page;
        this.itemsPerPage = data.per_page;
        this.totalPages = data.last_page;
        this.isPaginating = false;
      })
      .catch((err) => {
        this.isPaginating = false;
        this.$toasted.error("unable to fetching feedback survey");
      });
  }

  updateTable(options: any) {
    const page = options.page;
    if (page > this.currentPage || page < this.currentPage) {
      this.currentPage = page;
      this.getDataFromApi()
        .then((resp: any) => {
          this.data = resp.data.data;
          // console.log(this.data);
        })
        .catch((error: any) => {
          this.$toasted.error("unable to fetching feedback survey");
        });
    }
  }

  initTable(){
    this.getDataFromApi()
        .then((resp: any) => {
          this.data = resp.data;
          // console.log(this.data);
        })
        .catch((error: any) => {
          this.$toasted.error("unable to fetching feedback survey");
        });
  }

  getAnalytics(){
    axios
      .get("/consumer/load-referrals")
      .then((resp: any)=>{
        let data = resp.data.data;
        this.total_ref = data.total_ref;
        this.pending_points = data.pending_points;
        this.number_ref_points = data.number_ref_points;
      })
      .catch((err: any)=>{
      });
  }

}
