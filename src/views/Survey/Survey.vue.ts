import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class Survey extends Vue {
  @Prop() private msg!: string;
}
