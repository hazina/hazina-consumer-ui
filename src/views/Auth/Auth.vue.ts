import { Component, Vue } from "vue-property-decorator";
import Spinner from "@/components/Spinner/Spinner.vue";
import store from "@/store";
import CryptoJsAesJson from "@/CryptoJsAesJson";
import Axios from "axios";

@Component({
  components: {
    Spinner,
  },
})
export default class Auth extends Vue {
  //@Prop() private msg!: string;
  mounted() {
    Axios.get("/consumer/auth/authy/" + this.$route.params.id)
      .then((resp) => {
        store.dispatch("token_login", resp.data.token).then(() => {
          this.fetchUser();
          this.$toasted
            .success("You have logged in successfully!")
            .goAway(1500);
            this.$router.push({ name: "Home" });
        });
      })
      .catch((err) => {
        this.$toasted.error("Error fetching user!");
      });
  }

  fetchUser() {
    Axios.get("/consumer/user/profile")
      .then((res) => {
        store.commit("set_user", res.data.user);
      })
      .catch((res) => {})
      .finally(() => {});
  }
}
