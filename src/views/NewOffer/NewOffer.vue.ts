import { Component, Prop, Vue } from 'vue-property-decorator';
//import { VueperSlides, VueperSlide } from 'vueperslides'
var {VueperSlides, VueperSlide} = require("vueperslides");
import "vueperslides/dist/vueperslides.css";
import Axios from 'axios';
import Spinner from "@/components/Spinner/Spinner.vue";
import Empty from "@/components/Empty/Empty.vue";
 
@Component({
    components:{
        VueperSlides,
        VueperSlide,
        Spinner,
        Empty
    }
})
export default class NewOffer extends Vue {
 // @Prop() private msg!: string;
 new_offers : any = [];
 isLoading: boolean = false;

 init(){
     this.isLoading = true;
    Axios.get("/consumer/load-new-offers")
    .then((resp: any)=>{
        this.isLoading = false;
        this.new_offers = resp.data.data;
        //this.new_offers = [];
    })
    .catch((err)=>{
        this.isLoading = false;
       this.$toasted.error("Error fetching offers, please try again").goAway(500)
    });
 }

 mounted(){
     this.init();
 }

}
