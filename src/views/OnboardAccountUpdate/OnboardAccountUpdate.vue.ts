import { Component, Prop, Vue } from "vue-property-decorator";
import { required, minLength, sameAs, email } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";
import store from "@/store";
import moment from "moment";

@Component({
    validations: {
        username: { required, minLength: minLength(4) },
        firstname: { minLength: minLength(1) },
        lastname: { minLength: minLength(1) },
        gender: { required },
        state: { required },
        birthdate: { required }
    }
})
export default class OnboardAccountUpdate extends Vue {
    username: any = "";
    firstname: any = "";
    lastname: any = "";
    gender: any = "";
    state: any = "";
    birthdate: any = "";

    genders: any = [
        {
          title: "Male",
          value: "male"
        },
        {
          title: "Female",
          value: "female"
        },
        {
          title: "Others",
          value: "others"
        }
    ];
    
    states: any = [];

    accountSubmiting: boolean = false;
    fetchingStates: boolean = false;

    submitAccountForm() {
        this.$v.$touch();
        if (!this.$v.$invalid) {
            this.accountSubmiting = true;

            var formData = new FormData();
            formData.append("username", this.username);
            formData.append("firstname", this.firstname);
            formData.append("lastname", this.lastname);
            formData.append("gender", this.gender);
            formData.append("state", this.state);
            formData.append("age", this.birthdate);

            axios
                .post(
                    "/consumer/user/onboard-account-update",
                    formData,
                    {
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    }
                )
                .then(resp => {
                    this.accountSubmiting = false;
                    store.commit("set_user", resp.data.user);
                    this.$toasted
                        .success("account updated successfully!")
                        .goAway(10000);
                    this.$router.push({name: 'OnBoardingSurvey'});
                })
                .catch(err => {
                    // this.hasPasswordError = true;
                    this.accountSubmiting = false;
                    if (
                        _.has(err.response.data, "errors") &&
                        typeof err.response.data.errors != "string"
                    ) {
                        var field = _.keys(err.response.data.errors);
                        // this.passwordErrorMessages =
                        //     err.response.data.errors[field[0]][0];
                        this.$toasted
                            .error(err.response.data.errors[field[0]][0])
                            .goAway(2000);
                    } else {
                        // this.passwordErrorMessages = err.response.data.errors;
                        this.$toasted.error(err.response.data.errors);
                    }
                });
        }
    }

    fetchStates(){
        this.fetchingStates = true;
        axios.get("/consumer/user/fetch-states")
        .then((resp: any)=>{
            this.states = resp.data.states;
            this.fetchingStates = false;
        })
        .catch((err: any)=>{
            this.$toasted.error("Error fetching states").goAway(5000);
            this.fetchingStates = false;
        })
    }

    mounted(){
        this.fetchStates();
    }

    get max_date(){
        return moment().subtract(18, 'years').format("YYYY-MM-DD");
    }

    get min_date(){
        return moment().subtract(100, 'years').format("YYYY-MM-DD");
    }

    get usernameErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.username.$dirty) return errors;
        !val.username.minLength &&
            errors.push("Username must be at least 4 characters long");
        !val.username.required && errors.push("Username is required.");
        return errors;
    }

    get firstnameErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.firstname.$dirty) return errors;
        !val.firstname.minLength &&
            errors.push("firstname must be at least 4 characters long");
        // !val.firstname.required && errors.push("firstname is required.");
        return errors;
    }

    get lastnameErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.lastname.$dirty) return errors;
        !val.lastname.minLength &&
            errors.push("lastname must be at least 4 characters long");
        // !val.lastname.required && errors.push("lastname is required.");
        return errors;
    }

    get genderErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.gender.$dirty) return errors;
        !val.gender.required && errors.push("gender is required.");
        return errors;
    }

    get stateErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.state.$dirty) return errors;
        !val.state.required && errors.push("state is required.");
        return errors;
    }

    get birthdateErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.birthdate.$dirty) return errors;
        !val.birthdate.required && errors.push("birthdate is required.");
        return errors;
    }
}
