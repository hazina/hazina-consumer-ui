import { Component, Vue } from "vue-property-decorator";
import store from "@/store";
import Axios from "axios";

@Component
export default class App extends Vue {

    menu_children_slot_1: object = [
        {
            title: "Campaigns",
            icon: 'mdi-volume-high',
            children: [
                ["New Offer", "NewOffer", ''], //title,route_name,icon
                ["My Offer", "MyOffer", ''],
            ]
        },
        {
            title: "Earn Points",
            icon: 'mdi-diamond-outline',
            children: [
                ["Fun Surveys", "EarnPoints", ''], //title,route_name,icon
                ["Feedback Surveys", "FeedbackSurveys", ''],
                ["Refer & Earn", "Refer", ''],
            ]
        },

    ];

    menu_children_slot_2: object = [
        {
            title: "My Freebies",
            icon: 'mdi-heart-outline',
            children: [
                ["Product", "Products", ''], //title,route_name,icon
                ["Redeem Points", "RedeemPoints", '']
            ] 
        }
    ]; 

    

    mini: boolean =  false;

    drawer: any = null;

    minimizeMenu(){
        this.mini = !this.mini;
    }

    toggleMenu(){
        this.drawer = !this.drawer;
    }

    created() {
        var $this = this;
        (<any>this).$http.interceptors.response.use(undefined, function(
            err: any
        ) {
            return new Promise(function(resolve, reject) {
                if (
                    err.status === 401 &&
                    err.config &&
                    !err.config.__isRetryRequest
                ) {
                    $this.$store.dispatch("logout");
                }
                throw err;
            });
        });
        if (this.isLoggedIn) {
            this.fetchUser();
        }
    }

    LogOut() {
        store.dispatch("logout").then(() => {
            this.$toasted
                .success("You have logged out successfully!")
                .goAway(1000);
                this.$router.push({name: 'Logout'});
                //window.location.href = "/";
        });
    }

    fetchUser() {
        Axios.get("/consumer/user/profile")
            .then(res => {
                store.commit("set_user", res.data.user);
            })
            .catch(res => {})
            .finally(() => {});
    } 

    get isLoggedIn() {
        return store.getters.isLoggedIn;
    }

    get user() {
        return store.getters.user;
    }
}
