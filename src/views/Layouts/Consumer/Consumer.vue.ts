import { Component, Vue } from "vue-property-decorator";
import store from "@/store/index";
import Axios from "axios";
import { mdiAccountOutline, mdiDiamondStone, mdiHeartOutline, mdiHome, mdiHomeOutline, mdiMenu, mdiTruckFastOutline, mdiVolumeHigh } from '@mdi/js';

@Component
export default class Consumer extends Vue {

    home = mdiHome;
    home_o = mdiHomeOutline; 
    diamond = mdiDiamondStone;
    volume = mdiVolumeHigh;
    heart = mdiHeartOutline;
    menu = mdiMenu
    truck = mdiTruckFastOutline
    account = mdiAccountOutline

    menu_children_slot_1: object = [
        {
            title: "Campaigns",
            icon: this.volume,
            children: [
                ["New Offer", "NewOffer", ''], //title,route_name,icon
                ["My Offer", "MyOffer", ''],
            ]
        }
    ];

    menu_children_slot_2: object = [
        {
            title: "My Freebies",
            icon: this.heart,
            children: [
                ["Product", "Products", ''], //title,route_name,icon
                ["Redeem Points", "RedeemPoints", '']
            ]
        }
    ];

    

    mini: boolean =  false;

    drawer: any = null;

    minimizeMenu(){
        this.mini = !this.mini;
    }

    toggleMenu(){
        this.drawer = !this.drawer;
    }

    created() {
        var $this = this;
        (<any>this).$http.interceptors.response.use(undefined, function(
            err: any
        ) {
            return new Promise(function(resolve, reject) {
                if (
                    err.status === 401 &&
                    err.config &&
                    !err.config.__isRetryRequest
                ) {
                    $this.$store.dispatch("logout");
                }
                throw err;
            });
        });
        if (this.isLoggedIn) {
            this.fetchUser();
        }
    }

    LogOut() {
        store.dispatch("logout").then(() => {
            this.$toasted
                .success("You have logged out successfully!")
                .goAway(1000);
                this.$router.push({name: 'Home'});
                //window.location.href = "/";
        });
    }

    fetchUser() {
        Axios.get("/consumer/user/profile")
            .then(res => {
                store.commit("set_user", res.data.user);
            })
            .catch(res => {})
            .finally(() => {});
    }

    get isLoggedIn() {
        return store.getters.isLoggedIn;
    }
 
    get user() {
        return store.getters.user;
    }
}
