import { Component, Prop, Vue } from 'vue-property-decorator';
import CryptoJsAesJson from "@/CryptoJsAesJson";

@Component
export default class SurveyNotify extends Vue {
  points: string = "";
  type: string = "";

  mounted(){
    this.points = CryptoJsAesJson.prototype.Decrypt(this.$route.params.id);
    this.type = this.$route.params.type;
  }
}
