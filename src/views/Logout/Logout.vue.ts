import { Component, Prop, Vue } from 'vue-property-decorator';
import Spinner from "@/components/Spinner/Spinner.vue"
import store from "@/store";
@Component({
    components:{
        Spinner
    }
})
export default class Logout extends Vue {
  //@Prop() private msg!: string;
  mounted(){
    window.location.href = store.state.logoutUrl;
  }
}
 