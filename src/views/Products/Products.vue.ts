import { Component, Vue, Watch } from "vue-property-decorator";
var { VueperSlides, VueperSlide } = require("vueperslides");
import "vueperslides/dist/vueperslides.css";
import axios from "axios";
import Spinner from "@/components/Spinner/Spinner.vue";
import Empty from "@/components/Empty/Empty.vue";
import * as _ from "lodash";

@Component({
    components: {
        VueperSlides,
        VueperSlide,
        Spinner,
        Empty 
    }
})
export default class Products extends Vue {

    @Watch("options", { deep: true })
    
    totalData: number = 0;
    data: Array<object> = [];
    currentPage: number = 1;
    itemsPerPage: number = 0;
    loading: boolean = true;
    totalPages: number = 0;

    //options: any = {};
    headers: any = [
        {
            text: "",
            align: "start",
            sortable: false,
            //filterable: true,
            value: "inbound_inventory"
        },
        {
            text: "Name",
            sortable: false, 
            //filterable: true,
            value: "inbound_inventory.campaign.title"
        },
        // { text: "Brand", value: "brand.name" },
        // { text: "Ends On", value: "ends_on" },
    ];

    getCampaign() {
        this.getDataFromApi()
            .then((resp: any) => {
                this.data = resp.data.data;
                // console.log(this.data);
            })
            .catch((error: any) => {
                this.$toasted.error("unable to fetch data").goAway(10000);
            });
    }

    Paginate(page: any) {
        this.currentPage = page;
        this.getCampaign();
    }

    updateBySearch() {
        this.getDataFromApi()
            .then((resp: any) => {
                this.data = resp.data.data;
                // console.log(this.data);
            })
            .catch((error: any) => {
                this.$toasted.error("unable to fetch data").goAway(10000);
            });
    }

    mounted() {
        this.getCampaign();
    }

    updateTable(options: any) {
        //console.log(options);
        const page = options.page;
        if (page > this.currentPage || page < this.currentPage) {
            this.currentPage = page;
            this.getDataFromApi()
                .then((resp: any) => {
                    this.data = resp.data.data;
                    // console.log(this.data);
                })
                .catch((error: any) => {
                    this.$toasted.error("unable to fetch data").goAway(10000);
                });
        }
    }

    getDataFromApi() {
        this.loading = true;
        return new Promise((resolve, reject) => {
            // const { sortBy, sortDesc, page, itemsPerPage } = this.options
            axios
                .get("/consumer/load-my-products", {
                    params: { page: this.currentPage }
                })
                .then((resp: any) => {
                    let data = resp.data.data;
                    let items = data;
                    const total = data.total;
                    this.totalData = data.total;
                    this.currentPage = data.current_page;
                    this.itemsPerPage = data.per_page;
                    this.totalPages = data.last_page;
                    //console.log(data);
                    resolve({ data: items, total_data: data.total });
                    this.loading = false;
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
}
