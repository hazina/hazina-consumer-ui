import { Component, Prop, Vue } from "vue-property-decorator";
import axios from "axios";
import * as _ from "lodash";
import Spinner from "@/components/Spinner/Spinner.vue";
import Empty from "@/components/Empty/Empty.vue";
import CryptoJsAesJson from "@/CryptoJsAesJson";

@Component({
  components: {
    Spinner,
    Empty,
  },
}) 
export default class FeedbackSurveys extends Vue { 
  feedbackLoading: boolean = false;
  feedbacks: any = [];

  getFeedbackSurvey(){
    this.feedbackLoading = true;
    axios
      .get("/consumer/load-earn-points/feedback")
      .then((resp: any) => {
        this.feedbacks = resp.data.data.feedback_surveys;

      })
      .catch((err: any) => {
        this.$toasted.error("unable to fetching daily survey").goAway(5000);
      })
      .finally(()=>{
        this.feedbackLoading = false;
      })
  }

  encrypt(str: any) {
    return CryptoJsAesJson.prototype.Encrypt(str);
  }

  mounted(){
    this.getFeedbackSurvey();
  }


}
