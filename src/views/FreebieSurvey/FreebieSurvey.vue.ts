import { Component, Prop, Vue } from "vue-property-decorator";
import Axios from "axios";
import Spinner from "@/components/Spinner/Spinner.vue";
import * as Survey from "survey-knockout";
import store from "@/store";
import CryptoJsAesJson from "@/CryptoJsAesJson";

require("survey-knockout/modern.css");
require("survey-knockout/survey.min.css");

@Component({
  components: {
    Spinner,
  },
})
export default class FreebieSurvey extends Vue {
  survey: any = {};
  isLoading: boolean = false;
  isSaving: boolean = false;
  set_survey: any;
  survey_id: any = "";
  ID: any = "";

  fetchSurveys(id: any) {
    this.isLoading = true;
    Axios.get("/consumer/load/freebie/survey/" + id)
      .then((resp: any) => {
        this.survey = resp.data.survey.survey;
        this.survey_id = resp.data.survey.id;
        this.isLoading = false;
        if (resp.data.is_taken == true) {
          this.$toasted
            .info("Oops! you have already taken this daily survey!")
            .goAway(5000);
          this.$router.push({ name: "EarnPoints" });
        } else {
          Survey.StylesManager.applyTheme("orange");
          this.set_survey = new Survey.Survey(this.survey, "SurveyBox");
          this.set_survey.onComplete.add(this.sendDataToServer);
          this.set_survey.showCompletedPage = false;
        }
      })
      .catch((err) => {
        console.log(err);
        this.$toasted
          .error("Error fetching survey, please try reloading the page")
          .goAway(10000);
        this.isLoading = false;
      });
  }

  mounted() {
    this.ID = CryptoJsAesJson.prototype.Decrypt(this.$route.params.id);
    this.fetchSurveys(this.ID);
  }

  sendDataToServer() {
    this.isSaving = true;
    let resultAsString = JSON.stringify(this.set_survey.data);
    Axios.post("/consumer/freebie/save-survey", {
      survey: resultAsString,
      id: this.ID,
    })
      .then((resp: any) => {
        this.isSaving = false;
        store.commit("set_user", resp.data.user);
        let ID = CryptoJsAesJson.prototype.Encrypt(resp.data.points);
        this.$router.push({
          name: "SurveyNotify",
          params: { id: ID, type: "daily" },
        });
      })
      .catch((err) => {
        this.isSaving = false;
      });
  }
}
