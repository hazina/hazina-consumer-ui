import { Component, Vue, Watch } from "vue-property-decorator";
import {
  required,
  minLength,
  sameAs,
  email,
} from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";
import store from "@/store";
import {
  mdiCalendarMonth,
  mdiClose, 
  mdiFlipHorizontal,
  mdiFlipVertical,
} from "@mdi/js";
import moment from "moment";
import VImageInput from 'vuetify-image-input/a-la-carte';

@Component({ 
  validations: {
    accountForm: {
      username: { required, minLength: minLength(4) },
      firstname: { minLength: minLength(4) },
      lastname: { minLength: minLength(4) },
      //phone: { required },
      gender: { required },
      state: { required },
      birthdate: { required },
      // account_number: {
      //   required,
      //   minLength: minLength(10),
      //   maxLength: maxLength(10),
      // },
      // account_name: { required },
      // bank: { required },
    },
    passwordForm: {
      old_password: { required, minLength: minLength(6) },
      new_password: {
        required,
        minLength: minLength(6),
        sameAs: sameAs("confirm_password"),
      },
      confirm_password: {
        required,
        minLength: minLength(6),
        sameAs: sameAs("new_password"),
      },
    },
    emailForm: {
      //  old_email: { required, email },
      new_email: { required, email },
    },
  },
  components: {
    VImageInput
  },
})
export default class Profile extends Vue {
  //@Prop() private msg!: string;
  accountSubmiting: boolean = false;
  passwordSubmiting: boolean = false;
  emailSubmiting: boolean = false;

  Datemodal: boolean = false;

  hasPasswordError: boolean = false;
  passwordErrorMessages: string = "";
  data: any = "";
  modal: any = "";

  genders: any = [
    {
      title: "Male",
      value: "male"
    },
    {
      title: "Female",
      value: "female"
    },
    {
      title: "Others",
      value: "others"
    }
  ];

  states: any = [];

  fetchingStates: boolean = false;

  imageDialog: boolean = false;

  accountForm: any = {
    username: "",
    firstname: "",
    lastname: "",
    //phone: "",
    gender: "",
    state: "",
    birthdate: "",
    //account_number: "",
    //account_name: "",
   // bank: "",
    photo: [],
  };

  passwordForm: any = {
    old_password: "",
    new_password: "",
    confirm_password: "",
  };

  emailForm: any = {
    old_email: "",
    new_email: "",
  };

  images: any = [];
  activeImageUploads: any = {};
  placeholderImage: string = "";

  imageData: any = "";

  defaultImg: any = "";
  mime_type: any = "";
  dialog: boolean = false;

  calendarIcon = mdiCalendarMonth;
  clearIcon = mdiClose;
  fliphIcon = mdiFlipHorizontal;
  flipvIcon = mdiFlipVertical;

//   banks: any = [
//     'First Bank Nigeria Plc'
//   ];

//   banks : any = [
//     {'name' : 'Access Bank','code':'044'},
//     {'name' : 'Citibank','code':'023'},
//     {'name' : 'Diamond Bank','code':'063'},
//     {'name' : 'Dynamic Standard Bank','code':''},
//     {'name' : 'Ecobank Nigeria','code':'050'},
//     {'name' : 'Fidelity Bank Nigeria','code':'070'},
//     {'name' : 'First Bank of Nigeria','code':'011'},
//     {'name' : 'First City Monument Bank','code':'214'},
//     {'name' : 'Guaranty Trust Bank','code':'058'},
//     {'name' : 'Heritage Bank Plc','code':'030'},
//     {'name' : 'Jaiz Bank','code':'301'},
//     {'name' : 'Keystone Bank Limited','code':'082'},
//     {'name' : 'Providus Bank Plc','code':'101'},
//     {'name' : 'Polaris Bank','code':'076'},
//     {'name' : 'Stanbic IBTC Bank Nigeria Limited','code':'221'},
//     {'name' : 'Standard Chartered Bank','code':'068'},
//     {'name' : 'Sterling Bank','code':'232'},
//     {'name' : 'Suntrust Bank Nigeria Limited','code':'100'},
//     {'name' : 'Union Bank of Nigeria','code':'032'},
//     {'name' : 'United Bank for Africa','code':'033'},
//     {'name' : 'Unity Bank Plc','code':'215'},
//     {'name' : 'Wema Bank','code':'035'},
//     {'name' : 'Zenith Bank','code':'057'},
// ]

  @Watch("user")
  onUserChanged(val: any, oldval: any) {
    this.setFormInputs(val);
  }

  imageUpdated(evt: any) {
    console.log(evt);
  }

  submitAccountForm() {
    this.$v.accountForm.$touch();
    // this.accountSubmiting = true;
    if (!this.$v.accountForm.$invalid) {
      this.accountSubmiting = true;
      //let Imagge: any = this.convertURIToImageData(this.imageData);
      var formData = new FormData();
      formData.append("username", this.accountForm.username);
      formData.append("firstname", this.accountForm.firstname);
      formData.append("lastname", this.accountForm.lastname);
      formData.append("phone", this.accountForm.phone);
      formData.append("gender", this.accountForm.gender);
      formData.append("state", this.accountForm.state);
      formData.append("age", this.accountForm.birthdate);
     // formData.append("account_number", this.accountForm.account_number);
     // formData.append("account_name", this.accountForm.account_name);
     // formData.append("bank", this.accountForm.bank);
      if (this.imageData.length) {
        formData.append("photo", this.dataURItoBlob(this.imageData));
      }

      axios
        .post("/consumer/user/basic-settings", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        })
        .then((resp) => {
          this.accountSubmiting = false;
          this.imageData = "";
          store.commit("set_user", resp.data.user);
          this.$toasted.success("account updated successfully!").goAway(2000);
        })
        .catch((err) => {
          // this.hasPasswordError = true;
          this.accountSubmiting = false;
          if (
            _.has(err.response.data, "errors") &&
            typeof err.response.data.errors != "string"
          ) {
            var field = _.keys(err.response.data.errors);
            // this.passwordErrorMessages =
            //     err.response.data.errors[field[0]][0];
            this.$toasted
              .error(err.response.data.errors[field[0]][0])
              .goAway(2000);
          } else {
            // this.passwordErrorMessages = err.response.data.errors;
            this.$toasted.error(err.response.data.errors);
          }
        });
    }
  }

  submitPasswordForm() {
    this.$v.passwordForm.$touch();
    if (!this.$v.passwordForm.$invalid) {
      this.passwordSubmiting = true;
      let data = {
        old_password: this.passwordForm.old_password,
        new_password: this.passwordForm.new_password,
        confirm_password: this.passwordForm.confirm_password,
      };
      axios
        .post("/consumer/user/change-password", data)
        .then((resp) => {
          this.passwordSubmiting = false;
          this.$toasted.success("password updated successfully!").goAway(2000);
          this.$v.passwordForm.$reset;
        })
        .catch((err) => {
          // this.hasPasswordError = true;
          this.passwordSubmiting = false;
          if (
            _.has(err.response.data, "errors") &&
            typeof err.response.data.errors != "string"
          ) {
            var field = _.keys(err.response.data.errors);
            // this.passwordErrorMessages =
            //     err.response.data.errors[field[0]][0];
            this.$toasted
              .error(err.response.data.errors[field[0]][0])
              .goAway(2000);
          } else {
            // this.passwordErrorMessages = err.response.data.errors;
            this.$toasted.error(err.response.data.errors);
          }
        });
    }
  }

  submitEmailForm() {
    this.$v.emailForm.$touch();
    if (!this.$v.emailForm.$invalid) {
      this.emailSubmiting = true;
      let data = {
        email: this.emailForm.new_email,
      };
      axios
        .post("/consumer/user/change-email", data)
        .then((resp) => {
          this.emailSubmiting = false;
          this.$toasted
            .success(
              "Great! we have sent an email verification link to the new email you provided. Your email would be updated once you verify."
            )
            .goAway(10000);
        })
        .catch((err) => {
          // this.hasPasswordError = true;
          this.emailSubmiting = false;
          if (
            _.has(err.response.data, "errors") &&
            typeof err.response.data.errors != "string"
          ) {
            var field = _.keys(err.response.data.errors);
            // this.passwordErrorMessages =
            //     err.response.data.errors[field[0]][0];
            this.$toasted
              .error(err.response.data.errors[field[0]][0])
              .goAway(2000);
          } else {
            // this.passwordErrorMessages = err.response.data.errors;
            this.$toasted.error(err.response.data.errors);
          }
        });
    }
  }

  get user() {
    return store.getters.user;
  }

  setFormInputs(user: any) {
    this.emailForm.old_email = user.email;

    //account form setup
    this.accountForm.username = user.username;
    this.accountForm.firstname = user.firstname;
    this.accountForm.lastname = user.lastname;
    //this.accountForm.phone = user.phone;
    this.accountForm.gender = user.gender;
    this.accountForm.state = user.state;
    this.accountForm.birthdate = user.birthdate; 
    //this.accountForm.account_number = (user.account_number == null) ? "" : user.account_number;
   // this.accountForm.account_name = (user.account_name == null) ? "" : user.account_name;
   // this.accountForm.bank = (user.bank == null) ? "" : user.bank;
    //this.imageData = user.image;
  }

  fetchStates() {
    this.fetchingStates = true;
    axios
      .get("/consumer/user/fetch-states")
      .then((resp: any) => {
        this.states = resp.data.states;
        this.fetchingStates = false;
      })
      .catch((err: any) => {
        this.$toasted.error("Error fetching states").goAway(5000);
        this.fetchingStates = false;
      });
  }

  // onFileSelect(e: any) {
  //   const file = e.target.files[0]
  //   this.mime_type = file.type
  //   //console.log(this.mime_type)
  //   if (typeof FileReader === 'function') {
  //     this.dialog = true
  //     const reader = new FileReader()
  //     reader.onload = (event) => {
  //       //this.selectedFile = event.target.result
  //       //this.$refs.cropper.replace(this.selectedFile)

  //     }
  //     reader.readAsDataURL(file)
  //   } else {
  //     alert('Sorry, FileReader API not supported')
  //   }
  // }


  mounted() {
    this.fetchStates();
    this.setFormInputs(this.user);
  }

  get max_date(){
    return moment().subtract(18, 'years').format("YYYY-MM-DD");
}

get min_date(){
    return moment().subtract(100, 'years').format("YYYY-MM-DD");
}

  get usernameErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.accountForm.username.$dirty) return errors;
    !val.accountForm.username.minLength &&
      errors.push("Username must be at least 4 characters long");
    !val.accountForm.username.required && errors.push("Username is required.");
    return errors;
  }

  get firstnameErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.accountForm.firstname.$dirty) return errors;
    !val.accountForm.firstname.minLength &&
      errors.push("firstname must be at least 4 characters long");
    // !val.accountForm.firstname.required && errors.push("firstname is required.");
    return errors;
  }

  get lastnameErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.accountForm.lastname.$dirty) return errors;
    !val.accountForm.lastname.minLength &&
      errors.push("lastname must be at least 4 characters long");
    // !val.accountForm.lastname.required && errors.push("lastname is required.");
    return errors;
  }

  get phoneErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.accountForm.phone.$dirty) return errors;
    !val.accountForm.phone.required && errors.push("phone is required.");
    return errors;
  }

  get accountNumberErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.accountForm.account_number.$dirty)
    !val.accountForm.account_number.required &&
      errors.push("account number is required.")
    !val.accountForm.account_number.minLength &&
      errors.push("account number must be at least 10 characters long")
    !val.accountForm.account_number.maxLength &&
      errors.push("account number cannot be more than 10 characters long");
    return errors;
  }

  get accountNameErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.accountForm.account_name.$dirty)
    !val.accountForm.account_name.required &&
      errors.push("account name is required.");
    return errors;
  }

  get bankErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.accountForm.bank.$dirty) return errors;
    !val.accountForm.bank.required && errors.push("bank is required.");
    return errors;
  }

  get genderErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.accountForm.gender.$dirty) return errors;
    !val.accountForm.gender.required && errors.push("gender is required.");
    return errors;
  }

  get stateErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.accountForm.state.$dirty) return errors;
    !val.accountForm.state.required && errors.push("state is required.");
    return errors;
  }

  get birthdateErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.accountForm.birthdate.$dirty) return errors;
    !val.accountForm.birthdate.required &&
      errors.push("birthdate is required.");
    return errors;
  }

  get photoErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.accountForm.photo.$dirty) return errors;
    !val.accountForm.photo.required && errors.push("photo is required.");
    return errors;
  }

  get oldPasswordErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.passwordForm.old_password.$dirty) return errors;
    !val.passwordForm.old_password.required &&
      errors.push("old password is required.");
    return errors;
  }

  get confirmPasswordErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.passwordForm.confirm_password.$dirty) return errors;
    !val.passwordForm.confirm_password.required &&
      errors.push("You must confirm your password.");
    !val.passwordForm.confirm_password.minLength &&
      errors.push("Password must be at least 4 characters long");
    !val.passwordForm.confirm_password.sameAs &&
      errors.push("Passwords do not match");

    return errors;
  }

  get newPasswordErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.passwordForm.new_password.$dirty) return errors;
    !val.passwordForm.new_password.required &&
      errors.push("You must provide a new password.");
    !val.passwordForm.new_password.minLength &&
      errors.push("Password must be at least 4 characters long");
    !val.passwordForm.new_password.sameAs &&
      errors.push("Passwords do not match");

    return errors;
  }

  get newEmailErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.emailForm.new_email.$dirty) return errors;
    !val.emailForm.new_email.email && errors.push("Must be valid e-mail");
    !val.emailForm.new_email.required && errors.push("E-mail is required");
    return errors;
  }

  // get oldEmailErrors() {
  //     const errors: Array<string> = [];
  //     const val: any = this.$v;
  //     if (!val.emailForm.old_email.$dirty) return errors;
  //     !val.emailForm.old_email.old_email &&
  //         errors.push("Email Must be valid e-mail");
  //     !val.emailForm.old_email.required &&
  //         errors.push("Old E-mail is required");
  //     return errors;
  // }

  dataURItoBlob(dataURI: any) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(",")[1]);

    // separate out the mime component
    var mimeString = dataURI
      .split(",")[0]
      .split(":")[1]
      .split(";")[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);

    // create a view into the buffer
    var ia = new Uint8Array(ab);

    // set the bytes of the buffer to the correct values
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    var blob = new Blob([ab], { type: mimeString });
    return blob;
  }
}
