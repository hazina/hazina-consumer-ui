import Axios from 'axios';
import { Component, Vue } from "vue-property-decorator";
import store from "@/store/index";
import Spinner from "@/components/Spinner/Spinner.vue";

@Component({
    components:{
      Spinner
    }
})
export default class Thanks extends Vue {

      product_slides: number = 0;

      isLoading: boolean = false;
      isAccepting: boolean = false; 

      campaign: any = {};
      page: any = {};

      dialog: any = false;
      dialogType: any = "ACCEPT";
      can_accept: boolean = false;

      AcceptOfferConfirm(){
        if(!this.isLoggedIn){
            this.dialog = true;
            this.dialogType = "LOGIN";
        }else{
          this.dialog = true;
          this.dialogType = "ACCEPT";
        }
      }

      AcceptOffer(){
        this.dialog = false;
          this.isAccepting = true;
          Axios.post("/consumer/accept-offer/"+this.$route.params.id)
          .then((resp)=>{
              this.isAccepting = false;
              this.$toasted.success("Campaign has been successfully added to your offers").goAway(1000)
              this.$router.push({name: "Home"}); 
          })
          .catch((err)=>{
            this.isAccepting = false;
            this.$toasted.error(err.response.data.errors);
          })

      }

      loadOffer(){
        Axios.get("/consumer/campaign/load-offer/"+this.$route.params.id)
        .then((resp)=>{
          this.campaign = resp.data.data;
          this.page = JSON.parse(resp.data.data.page.data);
          this.can_accept = resp.data.can_accept;
        })
        .catch((err)=>{
          this.$toasted.error("Error loading Campaign").goAway(1000)
          this.$router.push({name: "Home"});
        })
      }

      get isLoggedIn() {
        return store.getters.isLoggedIn;
    }


      mounted(){ 
        this.loadOffer();
      }

}
