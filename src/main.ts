import Vue from 'vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import Axios from 'axios'
import Vuelidate from 'vuelidate'
import Toasted from 'vue-toasted'
import App from './views/App/App.vue';
import NProgress from 'vue-nprogress'

import VueTelInputVuetify from "vue-tel-input-vuetify/lib";
import VueCountdownTimer from 'vuejs-countdown-timer'
import VuetifyGoogleAutocomplete from 'vuetify-google-autocomplete';
import VueSocialSharing from 'vue-social-sharing'

Vue.config.productionTip = false


Vue.use(VuetifyGoogleAutocomplete, {
  apiKey: 'AIzaSyCryox6xfSrAJRW_PtbEXLc70c_mLaUB6M', // Can also be an object. E.g, for Google Maps Premium API, pass `{ client:
});

Vue.use(VueTelInputVuetify, {
    vuetify,
});
 
Vue.use(VueCountdownTimer)
Vue.use(VueSocialSharing);

const Noptions = {
  latencyThreshold: 200, // Number of ms before progressbar starts showing, default: 100,
  router: true, // Show progressbar when navigating routes, default: true
  http: false // Show progressbar when doing Vue.http, default: true
};

Vue.use(NProgress, Noptions)
const nprogress = new NProgress()

Vue.use(Vuelidate);
Vue.use(Toasted); 

Vue.prototype.$http = Axios;
const token = localStorage.getItem('haziAuth')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer '+token
} 

Vue.prototype.$http.defaults.headers.common['Haz-Authorization'] = 'Bearer K3r1WjgalH7Og4A0aHL1nV7bk0sTT09aoq7SnAAS2ir1XOH22GBgg3QU8CUs';
Vue.prototype.$http.defaults.baseURL = (process.env.NODE_ENV === "production") ? 'https://api.gethazina.com/v1' : 'https://hazinang.dev/v1';
// Vue.prototype.$http.defaults.headers.common['Haz-Authorization'] = 'Bearer K3r1WjgalH7Og4A0aHL1nV7bk0sTT09aoq7SnAAS2ir1XOH22GBgg3QU8CUs';
// Vue.prototype.$http.defaults.baseURL = (process.env.NODE_ENV === "production") ? 'https://api.hazina.xyz/v1' : 'https://hazinang.dev/v1';

// Vue.prototype.$http.defaults.headers.common['Haz-Authorization'] = 'Bearer amfmw136hC1RjhdZCamMeiN8x349utATvrcXWJo8vfUQRKHYnGGxJQLxQ9JI';
// Vue.prototype.$http.defaults.baseURL = 'https://hazinang.dev/v1';

new Vue({ 
  router,
  store,
  vuetify, 
  render: h => h(App) 
}).$mount('#app')
  