module.exports = {
  "transpileDependencies": [
    "vuetify",
    "vue-tel-input-vuetify"
  ],
  publicPath: "/",
  // pages:{
  //   ///title: 'hazina-consumer-portal'
  // },
  pwa:{
    name: 'Hazina',
    themeColor: '#f5a72b',
    msTileColor: '#ffffff',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
  },
  // scss: [
  //   'src/scss/variables.scss'
  // ]
}